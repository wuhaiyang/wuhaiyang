const express = require("express");
const router = express.Router();
//node request模块安装命令：npm install request
const request = require("request");
const querystring = require("querystring");
const Post = require("../../models/Post");
const User = require("../../models/User");
const cities = require("../../data/cities.json");

// @route   POST api/posts/code_send
// @desc    发送短信验证码  云之讯平台
// @access  Public
router.post("/sms_send", (req, res) => {
  // 存储
  Post.findOne({ phone: req.body.phone }).then((post) => {
    if (post) {
      res.json({
        msg: `验证码: ${post.code},该手机号以获取过验证码,请不要重复获取! `,
      });
    } else {
      let code = ("000000" + Math.floor(Math.random() * 999999)).slice(-6);
      const queryData = {
        sid: "70e9281073fe5e1546b7f657cae73897",
        token: "d1a5431e19d2005c3286794786111d79",
        appid: "099a1aedcf19400a9eb83d857eb7b845",
        templateid: "525851",
        param: code,
        mobile: req.body.phone,
      };
      const queryUrl = `https://open.ucpaas.com/ol/sms/sendsms`;
      request(
        {
          url: queryUrl,
          method: "POST",
          headers: {
            //设置请求头
            "content-type": "application/json",
          },
          body: JSON.stringify(queryData),
        },
        function (error, response, body) {
          const jsonObj = JSON.parse(body); // 解析接口返回的JSON内容
          if (jsonObj.code == "000000" && jsonObj.msg == "OK") {
            res.json(jsonObj);
            // 存储
            Post.findOne({ phone: req.body.phone }).then((post) => {
              if (!post) {
                new Post({
                  phone: req.body.phone,
                  code: code,
                }).save();
              } else {
                post.code = code;
                post.save();
              }
            });
          } else {
            console.log("请求异常", error);

            res.json(error);
          }
        }
      );
    }
  });
});

// @route   POST api/posts/sms_back
// @desc    验证短信验证码
// @access  Public
router.post("/sms_back", (req, res) => {
  Post.findOne({ phone: req.body.phone }).then((post) => {
    if (post) {
      if (post.code == req.body.code) {
        // res.json({ msg: '验证成功' })
        User.findOne({ phone: req.body.phone }).then((user) => {
          if (!user)
            new User({
              phone: req.body.phone,
            })
              .save()
              .then((user) => res.json({ msg: "验证成功", user }));
          else res.json({ msg: "验证成功", user });
        });
      } else res.status(404).json({ msg: "验证码有误" });
    }
  });
});

// @route   POST api/posts/cities
// @desc    获取城市数据
// @access  Public
router.get("/cities", (req, res) => {
  res.json(cities);
});

module.exports = router;

const express = require("express");
const mongoose = require("mongoose");
const posts = require("./routes/api/posts");
const profiles = require("./routes/api/profile");
const users = require("./routes/api/user");
const bodyParser = require("body-parser");

const app = express();
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

// DB Config
// const db = require("./config/keys").mongoURI;

// const mongoose = require("mongoose");
const db =
  "mongodb://laowuweb:laowuweb@localhost:27017/laowuweb?authSource=admin";

// Connect to MongoDB
mongoose
  .connect(db, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => console.log("MongoDB Connected"))
  .catch((err) => console.log(err));

// Use Routes
app.use("/api/posts", posts);
app.use("/api/profile", profiles);
app.use("/api/user", users);

const port = process.env.PORT || 5001;

app.listen(port, () => console.log(`Server running on port ${port}`));
